<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;

class CreateDataCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create Master Data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $database_seeder = new \DatabaseSeeder();
        $database_seeder->call(\CountriesTableSeeder::class);
        $database_seeder->call(\ProvincesTableSeeder::class);
        $database_seeder->call(\CitiesTableSeeder::class);
        $database_seeder->call(\CategoryAssetsTableSeeder::class);
        $database_seeder->call(\TrainingTypeTableSeeder::class);
        $database_seeder->call(\LeaveTypeSeeder::class);
        $database_seeder->call(\EmployeeStatusSeeder::class);
        $database_seeder->call(\JobPositionsSeeder::class);
        echo "Create Master Data Successfully";
    }
}
