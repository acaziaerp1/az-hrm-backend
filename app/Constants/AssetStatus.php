<?php

namespace App\Constants;

class ASSETSTATUS {
	const RETURNED = 1;
	const NEW = 0;
	const APPROVED = 10;

    public static function getLabel(int $status) {
        switch ($status) {
            case ASSETSTATUS::NEW:
                return  __('asset.new');
                break;
            case ASSETSTATUS::RETURNED:
                return  __('asset.returned');
                break;
            case ASSETSTATUS::APPROVED:
                return  __('asset.approved');
                break;
        }
    }
}
