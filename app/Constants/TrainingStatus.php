<?php

namespace App\Constants;

class TRAININGSTATUS {
	const NEW = 0;
	const CANCEL = 5;
	const DONE = 10;

    public static function getLabel(int $status) {
        switch ($status) {
            case TRAININGSTATUS::NEW:
                return  __('training.new');
                break;
            case TRAININGSTATUS::CANCEL:
                return  __('training.cancel');
                break;
            case TRAININGSTATUS::DONE:
                return  __('training.done');
                break;
        }
    }
}
