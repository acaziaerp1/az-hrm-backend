<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\City;
use App\Models\Province;
use App\Models\ApiResponse;
use Illuminate\Http\Request;

class AddressController extends Controller
{
         /**
     * @SWG\GET(
     *     path="/countries",
     *     summary="Get all countries",
     *     operationId="get-all-countries",
     *     tags={"Countries"},
     *     produces={"application/json"},
     *  
     *       @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function getCountries(ApiResponse $response){
        $countries = Country::get();
        $response->data = $countries;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

     /**
     * @SWG\GET(
     *     path="/provinces",
     *     summary="Get provinces",
     *     operationId="get-provinces",
     *     tags={"Provinces"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Country: Country Id",
     *         in="query",
     *         name="country_id",
     *         required=true,
     *         type="integer"
     *     ),
     *    
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function getProvinces(ApiResponse $response, Request $request){
        $country = $request->country_id;
        if(isset($country)){
            $provinces = Province::where('country_id', $country)->get();
        }
        $response->data = $provinces;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\GET(
     *     path="/cities",
     *     summary="Get cities",
     *     operationId="get-cities",
     *     tags={"Cities"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Province: Province Id",
     *         in="query",
     *         name="province_id",
     *         required=true,
     *         type="integer"
     *     ),
     *    
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function getCities(ApiResponse $response, Request $request){
        $province = $request->province_id;
        if(isset($province)){
            $cities = City::where('province_id', $province)->get();
        }
        $response->data = $cities;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }
}
