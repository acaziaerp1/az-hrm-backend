<?php

namespace App\Http\Controllers;

use App\Models\AssetModel;
use App\Models\CategoryAssets;
use Illuminate\Http\Request;
use App\User as UserModel;
use App\Models\ApiResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\GET(
     *     path="/assets",
     *     summary="Get all assets",
     *     operationId="get-asset",
     *     tags={"Assets"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="pageIndex: Current page of the query",
     *         in="query",
     *         name="pageIndex",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="pageSize: Limit of query",
     *         in="query",
     *         name="pageSize",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="EmployeeName",
     *         in="query",
     *         name="employee_name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="CategoryID",
     *         in="query",
     *         name="cate_asset_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *        security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function get(ApiResponse $response)
    {
        $pageIndex = request()->pageIndex;
        $pageSize = request()->pageSize;
        $employee_name = request()->employee_name;
        $cate_asset_id = request()->cate_asset_id;
        $query = AssetModel::with('employee','category')->orderBy('id', 'DESC');
        if (isset($employee_name)){
            $query->whereHas('employee', function ($sub_query) use($employee_name) {
                $sub_query->where('username', 'like', '%' . $employee_name . '%');
            });
        }
        if (isset($cate_asset_id)){
            $query->whereHas('category', function ($sub_query) use($cate_asset_id) {
                $sub_query->where('id', $cate_asset_id);
            });
        }
        if(isset($purchase_date_from)){
            $query->where('purchase_date','>=',$purchase_date_from);
        }
        if(isset($purchase_date_to)){
            $query->where('purchase_date','<=',$purchase_date_to);
        }
        $total = (clone $query);
        if (isset($pageIndex) && isset($pageSize)) {
            $query->offset($pageIndex * $pageSize)->limit($pageSize);
        }
        $data = $query->get();
        $response->data = $data->toArray();
        $response->total = $total->count();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/assets",
     *     summary="Insert/Update a assets",
     *     operationId="insert-update-asset",
     *     tags={"Assets"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID: ID of asset if update",
     *         in="formData",
     *         name="id",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Name: Name of asset",
     *         in="formData",
     *         name="name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Code: Code of asset",
     *         in="formData",
     *         name="code",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="SerialNumber: SerialNumber of asset",
     *         in="formData",
     *         name="serial_number",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Date Loaned of asset (Y-m-d)",
     *         in="formData",
     *         name="date_loaned",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Date Returned of asset (Y-m-d)",
     *         in="formData",
     *         name="date_returned",
     *         required=false,
     *         type="string"
     *     ),
     *       @SWG\Parameter(
     *         description="UserID: Belong to user_id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         description="CategoryID: Belong to category",
     *         in="formData",
     *         name="category_id",
     *         required=true,
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         description="Description: Description of asset",
     *         in="formData",
     *         name="description",
     *         required=true,
     *         type="string"
     *     ),
     *       @SWG\Parameter(
     *         description="Image: Base64 String",
     *         in="formData",
     *         name="image",
     *         required=false,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(Request $request,ApiResponse $response)
    {
        $request->validate([
            'id' => 'sometimes|required',
            'name' => 'required',
            'code' => 'sometimes|unique:assets,code,' . ($request->has('id') ? $request->id : 0),
            'description' => 'required',
            'category_id' => 'required',
            'user_id' => 'required|numeric',
            'date_loaned' => 'sometimes|nullable|date_format:Y-m-d',
            'date_returned' => 'sometimes|nullable|date_format:Y-m-d',
        ]);
        $data = $request->except('id');
        $id_asset = $request->get('id');
        if(!empty($data['image'])){
            try {
                if (substr($data['image'], 0, 10) == "data:image") {
                    $str = explode(";base64,", $data['image']);
                    $data['image'] = $str[1];
                    $filename = Str::random(10);
                    $content = base64_decode($data['image']);
                    $data['image'] = "images/assets/{$filename}.png";
                    Storage::disk('public')->put($data['image'], $content);
                }
            } catch (\Exception $e) {
                $response->upload_response = $e->getMessage();
            }
        }
        //Update check exits images
        if($id_asset){
            $asset_info = AssetModel::where('id',$id_asset)->first();
            if (!empty($asset_info->image) && Storage::disk('public')->exists($asset_info->getOriginal('image'))) {
                try {
                    Storage::disk('public')->delete($asset_info->getOriginal('image'));
                } catch (\Exception $e) {
                    $response->upload_response = $e->getMessage();
                }
            }
        }
        $res = AssetModel::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        if($res){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\DELETE(
     *     path="/assets/{id}",
     *     summary="delete assets",
     *     operationId="delete-assets",
     *     tags={"Assets"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of assets",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function destroy(ApiResponse $response, $id)
    {
        $asset = AssetModel::findOrFail($id);
        if($asset->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }

    /**
     * @SWG\GET(
     *     path="/assets/category",
     *     summary="get category assets",
     *     operationId="category-assets",
     *     tags={"Assets"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function category(ApiResponse $response)
    {
        $category_assets = CategoryAssets::get();
        $response->data = $category_assets;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\GET(
     *     path="/user/assets",
     *     summary="user assets",
     *     operationId="user-assets",
     *     tags={"Assets"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of user",
     *         in="query",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function userAssets(ApiResponse $response){
        request()->validate([
            'user_id' => 'required',
        ]);
        $user = UserModel::where('id', request()->user_id)->with('assets')->get();
        $response->data = $user->toArray();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/assets/updateStatus",
     *     summary="update status trainings",
     *     operationId="status-trainings",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of asset",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="status: trainings => 0 : New; 5 : Return; 10 : Approve",
     *         in="formData",
     *         name="status_id",
     *         required=true,
     *         type="integer",
     *          enum={0,5,10}
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function updateStatus(Request $request,ApiResponse $response)
    {
        $request->validate([
            'status_id' => 'numeric|required',
            'id' => 'numeric|required',
        ]);
        $update = AssetModel::where('id',$request->id)->update(['status' => $request->status_id]);
        if($update){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
