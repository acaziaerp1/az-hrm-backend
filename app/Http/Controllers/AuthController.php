<?php

namespace App\Http\Controllers;

use App\User;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use GuzzleHttp\Psr7\Request as RequestGuzzle;
use  GuzzleHttp\Psr7;
use GuzzleHttp\Client;
use App\Models\ApiResponse;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * @SWG\POST(
     *     path="/auth/authorized",
     *     summary="Authorize User",
     *     operationId="authorized-user",
     *     tags={"Auth"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Token: Token of AcaziaPortal",
     *         in="formData",
     *         name="token",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     * )
     */
    public function authorized(Request $request,ApiResponse $response)
    {
        $request->validate([
            'token' => 'required|string',
        ]);
        $token = $request->token;
        $client = new Client();
        try{
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ];
            $res = $client->request('GET', 'http://id.acaziasoft.com/v1/api/users/my-profile', [
               'headers' => $headers
            ]);

            $data = json_decode($res->getBody()->getContents(),true);
            if(!empty($data['data'])){
                $data_user = $data['data'];
                $res = Auth::loginUsingId($data['data']['id']);
                $user = Auth::guard()->user();
                if(!$res && !$user){
                    $user = new User([
                        'id' => $data_user['id'],
                        'username' => $data_user['full_name'],
                        'email' => $data_user['email'],
                        'gender' => $data_user['gender'],
                        'mobile_phone' => $data_user['phone'],
                        'employee_code' => 'HRMs-'.rand(1,999),
                        'created_at' => time(),
                    ]);
                    $user->save();
                }
                $tokenResult = $user->createToken("Personal Access Token");
                $token = $tokenResult->token;
                $token->save();
                $response->success = true;
                $response->message = __('auth.success');
                $response->data = array(
                    'user' => $user,
                    'access_token' => $tokenResult->accessToken,
                );
            }

        }catch(RequestException $e){
            $response->success = false;
            $response->message = $e->getMessage();
        }

        return response()->json($response, 200);
    }
}
