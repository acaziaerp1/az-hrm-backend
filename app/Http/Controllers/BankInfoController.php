<?php

namespace App\Http\Controllers;

use App\Models\BankInfo;
use App\User;
use Illuminate\Http\Request;
use App\Models\ApiResponse;
use Illuminate\Validation\Rule;

class BankInfoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /**
     * @SWG\GET(
     *     path="/bank-info",
     *     summary="Get user bank information",
     *     operationId="get-user-bank-information",
     *     tags={"Bank Information"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function index(ApiResponse $response)
    {
        request()->validate([
            'user_id' => 'required|exists:App\User,id'
        ]);
        $user =  User::find(request()->user_id)->bank_info;
        $response->data = $user;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

     /**
     * @SWG\POST(
     *     path="/bank-info",
     *     summary="Get bank information",
     *     operationId="get-bank-information",
     *     tags={"Bank Information"},
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *         description="id: id",
     *         in="formData",
     *         name="id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="user_id: User id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="Account number: account_nmuber",
     *         in="formData",
     *         name="account_number",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="Account name: account_name",
     *         in="formData",
     *         name="account_name",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="bank name: bank name",
     *         in="formData",
     *         name="bank_name",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="branch name: bank name",
     *         in="formData",
     *         name="bank_name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function store(ApiResponse $response, Request $request)
    {
        $bank_info = User::find($request->user_id)->bank_info;
        $request->validate([
            'id' => 'sometimes|required',
            'user_id' => [
                'required',
            ],
            'account_name' => [
                'required'
            ],
            'account_number' => [
                'required',
                Rule::unique('bank_infos', 'account_number')->ignore($request->id ? $request->id : 0),
            ],
            'bank_name' => 'required',
            'branch_name' => 'required',
        ]);
        $bankInfo = BankInfo::updateOrCreate(
            ['user_id' => $request->user_id],
            [
                'account_number' => $request->account_number, 
                'account_name' => $request->account_name, 
                'bank_name' => $request->bank_name, 
                'branch_name' => $request->branch_name
            ],
        );
        if ($bankInfo) {
            $response->data = $request->toArray();
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BankInfo  $bankInfo
     * @return \Illuminate\Http\Response
     */

     /**
     * @SWG\DELETE(
     *     path="/bank-info/{id}",
     *     summary="delete bank-information",
     *     operationId="delete-bank-information",
     *     tags={"bank-information"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of bank-information",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function destroy(ApiResponse $response, $id)
    {
        $bankInfo = bankInfo::findOrFail($id);
        if($bankInfo->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }
}
