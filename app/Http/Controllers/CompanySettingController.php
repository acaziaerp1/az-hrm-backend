<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\CompanySetting;
use App\Models\ApiResponse;

class CompanySettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     /**
     * @SWG\GET(
     *     path="/settings",
     *     summary="Show settings",
     *     operationId="get-settings",
     *     tags={"Settings"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function index(ApiResponse $response)
    {
        $companySettings = new CompanySetting();
        $companySettings = CompanySetting::with('city', 'country', 'province')->orderBy('id', 'DESC')->get();
        $response->data = $companySettings->toArray();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     /**
     * @SWG\POST(
     *     path="/settings",
     *     summary="Edit or create settings",
     *     operationId="edit-or-create-settings",
     *     tags={"Settings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Name: Name of Company",
     *         in="formData",
     *         name="name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="contact_person: Contact Person",
     *         in="formData",
     *         name="contact_person",
     *         required= true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="address: Company Address",
     *         in="formData",
     *         name="address",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="email: Company Email Address",
     *         in="formData",
     *         name="email",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="phone_number: Company Phone number",
     *         in="formData",
     *         name="phone_number",
     *         required=false,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="mobile_number: Mobile number",
     *         in="formData",
     *         name="mobile_number",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="fax: Company fax",
     *         in="formData",
     *         name="fax",
     *         required=false,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="website_url: Company website",
     *         in="formData",
     *         name="website_url",
     *         required=false,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="city_id: City",
     *         in="formData",
     *         name="city_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="province_id: Province",
     *         in="formData",
     *         name="province_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="country_id: Country",
     *         in="formData",
     *         name="country_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(Request $request, ApiResponse $response)
    {
        $request->validate([
            'name' => 'required',
            'contact_person' => 'required',
            'email' => 'required',
            'mobile_number' => 'required|between:10,10',
            'city_id' => 'required|numeric',
            'province_id' => 'required|numeric',
            'country_id' => 'required|numeric'
        ]);
        $count = CompanySetting::count();
        $settings = $request->isMethod('put') && ($count > 0) ? CompanySetting::findOrFail($request->settings_id) : new CompanySetting;
        $settings->id = $request->input('settings_id');
        $settings->name = $request->input('name');
        $settings->contact_person = $request->input('contact_person');
        $settings->address = $request->input('address');
        $settings->email = $request->input('email');
        $settings->phone_number = $request->input('phone_number');
        $settings->mobile_number = $request->input('mobile_number');
        $settings->fax = $request->input('fax');
        $settings->website_url = $request->input('website_url');
        $settings->city_id = $request->input('city_id');
        $settings->country_id = $request->input('country_id');
        $settings->province_id = $request->input('province_id');

        if($settings->save()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }

}
