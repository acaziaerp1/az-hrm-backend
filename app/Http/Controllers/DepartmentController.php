<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\Department;

class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    /**
     * @SWG\GET(
     *     path="/departments",
     *     summary="Get all departments",
     *     operationId="get-departments",
     *     tags={"Department"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="pageIndex: Current page of the query",
     *         in="query",
     *         name="pageIndex",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="pageSize: Limit of query",
     *         in="query",
     *         name="pageSize",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="name: Name of Department",
     *         in="query",
     *         name="name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function index(ApiResponse $response)
    {
        $pageIndex = request()->pageIndex;
        $pageSize = request()->pageSize;
        $name = request()->name;
        $departments = new Department();
        $query = $departments::with('head')->withCount('employee')->orderBy('id', 'DESC');
        if (isset($name)){
            $query->where('name', 'like', '%' . $name . '%');
        }
        $total = (clone $query);
        if (isset($pageIndex) && isset($pageSize)) {
            $query->offset($pageIndex * $pageSize)->limit($pageSize);
        }
        $data = $query->get();
        $response->data = $data->toArray();
        $response->total = $total->count();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

      /**
     * @SWG\POST(
     *     path="/department",
     *     summary="Edit or create department",
     *     operationId="edit-or-create-department",
     *     tags={"Department"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Name: Name of department",
     *         in="formData",
     *         name="name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Department_head: Head of Department",
     *         in="formData",
     *         name="department_head",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
       *      security={
       *       {"userToken": {}}
       *     }
     * )
     */

    /**
     * @SWG\PUT(
     *     path="/department",
     *     summary="Edit or create department",
     *     operationId="edit-or-create-department",
     *     tags={"Department"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Name: Name of department",
     *         in="formData",
     *         name="name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Department_head: Head of Department",
     *         in="formData",
     *         name="department_head",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="Department_id: ID of Department",
     *         in="formData",
     *         name="department_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */



    public function store(Request $request, ApiResponse $response)
    {
        $request->validate([
            'name' => 'required',
            'department_head' => 'required'
        ]);
        $department = $request->isMethod('put') ? Department::findOrFail($request->department_id) : new Department;

        $department->id = $request->input('department_id');
        $department->name = $request->input('name');
        $department->department_head = $request->input('department_head');

        if($department->save()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
            // return new HolidayResource($holiday);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function show($id)
    // {
    //     //
    // }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

         /**
     * @SWG\POST(
     *     path="/department/{id}",
     *     summary="delete department",
     *     operationId="delete-department",
     *     tags={"Department"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of department",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
      *      security={
      *       {"userToken": {}}
      *     }
     * )
     */

    public function destroy(ApiResponse $response, $id)
    {
        $department = Department::findOrFail($id);
        if($department->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
            // return new HolidayResource($holiday);
        }
    }
}
