<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\EmailSettingsModel as EmailSettings;

class EmailSettingsController extends Controller
{
    /**
     * @SWG\GET(
     *     path="/settings/email",
     *     summary="Show email settings",
     *     operationId="get-email-settings",
     *     tags={"Settings"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     * security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function index(ApiResponse $response){
        $emailSettings = EmailSettings::first();
        if($emailSettings){
            $response->data = $emailSettings->toArray();
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/settings/email",
     *     summary="Edit or create email settings",
     *     operationId="edit-or-create-email-settings",
     *     tags={"Settings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="smtp_host: SMTP Host",
     *         in="formData",
     *         name="smtp_host",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="smtp_user: SMTP User",
     *         in="formData",
     *         name="smtp_user",
     *         required= true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="smtp_password: SMTP Password",
     *         in="formData",
     *         name="smtp_password",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="smtp_port: SMTP Port",
     *         in="formData",
     *         name="smtp_port",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="smtp_security: SMTP Security",
     *         in="formData",
     *         name="smtp_security",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="smtp_auth_domain: SMTP Auth Domain",
     *         in="formData",
     *         name="smtp_auth_domain",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'smtp_host' => 'required',
            'smtp_user' => 'required',
            'smtp_password' => 'required',
            'smtp_port' => 'required',
            'smtp_security' => 'required|numeric',
            'smtp_auth_domain' => 'required',
        ]);
        $data = $request->except('id');
        $emailSettings = EmailSettings::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        if($emailSettings){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
