<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\EmployeeStatusModel as EmployeeStatus;
use App\Models\EmploymentStatusModel as EmploymentStatus;
use App\User;

class EmploymentStatusController extends Controller
{
    /**
     * @SWG\GET(
     *     path="/employment-status",
     *     summary="Get employment status",
     *     operationId="get-employment-status",
     *     tags={"Employment Status"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function get(ApiResponse $response){
        request()->validate([
            'user_id' => 'required | exists:App\User,id',
        ]);
        $user_id = request()->user_id;
        $employmentStatus = EmploymentStatus::where('user_id', $user_id)->with('user', 'status')->get();
        $response->data = $employmentStatus;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/employment-status",
     *     summary="Get employment status",
     *     operationId="get-employment-status",
     *     tags={"Employment Status"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="user_id: User id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="status_id: Employment status ID",
     *         in="formData",
     *         name="status_id",
     *         required= true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="effective_date: Effective Date",
     *         in="formData",
     *         name="effective_date",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="comment: Comment",
     *         in="formData",
     *         name="comment",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'status_id' => 'required',
            'effective_date' => 'required',
        ]);
        $data = $request->except('id');
        $employmentStatus = EmploymentStatus::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        if($employmentStatus){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
