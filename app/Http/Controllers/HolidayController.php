<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Holiday;
use App\Http\Requests;
use App\Http\Resources\Holiday as HolidayResource;
use App\Models\ApiResponse;
use Carbon\Carbon;

class HolidayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\GET(
     *     path="/holidays",
     *     summary="Get all holidays",
     *     operationId="get-holiday",
     *     tags={"Holiday"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="pageIndex: Current page of the query",
     *         in="query",
     *         name="pageIndex",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="pageSize: Limit of query",
     *         in="query",
     *         name="pageSize",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="name: Name of holiday",
     *         in="query",
     *         name="name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="date: Date of Holiday",
     *         in="query",
     *         name="date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function index(ApiResponse $response)
    {
        $pageIndex = request()->pageIndex;
        $pageSize = request()->pageSize;
        $name = request()->name;
        $date = request()->date;
        $week = request()->week;
        $year = date('Y', strtotime($date));
        $month = date('m', strtotime($date));
        $today = date('Y-m-d');
        $holidays = new Holiday();
        $query = $holidays->orderBy('id', 'DESC');
        if (isset($name)){
            $query->where('name', 'like', '%' . $name . '%');
        }
        if (isset($date)){
            $date = Carbon::parse(request()->date)->format('Y-m-d');
            $query->where('date', $date);
        }
        $total = (clone $query);
        if (isset($pageIndex) && isset($pageSize)) {
            $query->skip($pageIndex * $pageSize)->take($pageSize);
        }
//        if(isset($year)){
//            $query->whereYear('date', $year);
//        }
//        if (isset($month)){
//            $query->whereMonth('date', $month)->get();
//        }
//        if (isset($week)){
//            $query->whereBetween('date', [Carbon::now()->startOfWeek(),Carbon::now()->endOfWeek()]);
//        }
        $holidays = $query->get();
        $response->data = $holidays->toArray();
        $response->total = $total->count();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

     /**
     * @SWG\POST(
     *     path="/holiday",
     *     summary="Edit or create holiday",
     *     operationId="edit-or-create-holiday",
     *     tags={"Holiday"},
     *     produces={"application/json"},
      *     @SWG\Parameter(
     *         description="Name: Name of holiday",
     *         in="formData",
     *         name="name",
     *         required=true,
     *         type="string"
     *     ),
      *     @SWG\Parameter(
     *         description="Repeat: Repeat of holiday",
     *         in="formData",
     *         name="is_repeat",
     *         required=true,
     *         type="integer",
     *         enum={0,1},
     *     ),
     *     @SWG\Parameter(
     *         description="Date: Date of holiday",
     *         in="formData",
     *         name="date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
      *      security={
      *       {"userToken": {}}
      *     }
     * )
     */

    /**
     * @SWG\PUT(
     *     path="/holiday",
     *     summary="Edit or create holiday",
     *     operationId="edit-or-create-holiday",
     *     tags={"Holiday"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="HolidayID: Specific if want to edit",
     *         in="formData",
     *         name="holiday_id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Name: Name of holiday",
     *         in="formData",
     *         name="name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Repeat: Repeat of holiday",
     *         in="formData",
     *         name="is_repeat",
     *         required=false,
     *         type="integer",
     *         enum={0,1},
     *     ),
     *     @SWG\Parameter(
     *         description="Date: Date of holiday",
     *         in="formData",
     *         name="date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(Request $request,ApiResponse $response)
    {
        $request->validate([
            'name' => 'required',
            'date' => 'required|date',
        ]);
        $holiday = $request->isMethod('put') ? Holiday::findOrFail($request->holiday_id) : new Holiday;

        $holiday->id = $request->input('holiday_id');
        $holiday->name = $request->input('name');
        $holiday->date = $request->input('date');
        $holiday->is_repeat = $request->input('is_repeat');

        if($holiday->save()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
            // return new HolidayResource($holiday);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\DELETE(
     *     path="/holiday/{id}",
     *     summary="delete holiday",
     *     operationId="delete-holiday",
     *     tags={"Holiday"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of holiday",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function destroy(ApiResponse $response, $id)
    {
        $holiday = Holiday::findOrFail($id);
        if($holiday->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
            // return new HolidayResource($holiday);
        }
    }

}
