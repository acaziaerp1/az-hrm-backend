<?php

namespace App\Http\Controllers;

use App\Models\JobPositionsModel;
use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\JobInformationModel as JobInfo;
use App\User;


class JobInformationController extends Controller
{

    /**
     * @SWG\GET(
     *     path="/job-information",
     *     summary="Get user job information",
     *     operationId="get-user-job-information",
     *     tags={"Job Information"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function get(ApiResponse $response){
        request()->validate([
            'user_id' => 'required|exists:App\User,id',
        ]);
        $user_id = request()->user_id;
        $jobInfo = JobInfo::where('user_id', $user_id)->with(['reports_to', 'status'])->get();;
        $response->data = $jobInfo->toArray();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/job-information",
     *     summary="Get job information",
     *     operationId="get-job-information",
     *     tags={"Job Information"},
     *     produces={"application/json"},
     *      @SWG\Parameter(
     *         description="ID: ID of job info if update",
     *         in="formData",
     *         name="id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="user_id: User id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="location: location",
     *         in="formData",
     *         name="location",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="division: division",
     *         in="formData",
     *         name="division",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="department: department",
     *         in="formData",
     *         name="department",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="job_title: job_title",
     *         in="formData",
     *         name="job_title",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="effective_date: effective_date",
     *         in="formData",
     *         name="effective_date",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="Status ID: status_id",
     *         in="formData",
     *         name="status_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="reports_to: reports_to",
     *         in="formData",
     *         name="reports_to",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'id' => 'sometimes|required',
            'user_id' => 'required',
            'effective_date' => 'required',
            'location' => 'required',
            'division' => 'required',
            'department' => 'required',
            'job_title' => 'required',
            'status_id' => 'required',
            'reports_to' => 'required',
        ]);
        $data = $request->except('id');
        $jobInfo = JobInfo::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        if($jobInfo){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\DELETE(
     *     path="/job-information/{id}",
     *     summary="delete job-information",
     *     operationId="delete-job-information",
     *     tags={"Job Information"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of job-information",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function destroy(ApiResponse $response, $id){
        $jobInfo = JobInfo::findOrFail($id);
        if($jobInfo->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }

    /**
     * @SWG\GET(
     *     path="/job-title",
     *     summary="List job title",
     *     operationId="list-job-title",
     *     tags={"Job Information"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function getJobTitle(ApiResponse $response){
        $jobTypes = JobPositionsModel::select('id','name')->get();
        $response->data = $jobTypes;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }
}
