<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\LeaveModel as Leave;
use App\Models\LeaveTypeModel as LeaveType;

class LeaveController extends Controller
{
    /**
     * @SWG\GET(
     *     path="/leaves",
     *     summary="Get all leaves",
     *     operationId="get-leave",
     *     tags={"Leaves"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="pageIndex: Current page of the query",
     *         in="query",
     *         name="pageIndex",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="pageSize: Limit of query",
     *         in="query",
     *         name="pageSize",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="User name",
     *         in="query",
     *         name="user_name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function get(ApiResponse $response){
        $pageIndex = request()->pageIndex;
        $pageSize = request()->pageSize;
        $user = request()->user_id;
        $user_name = request()->user_name;
        $leaves = new Leave();
        $query = $leaves->orderBy('id', 'DESC');
        if (isset($user)){
            $query->where('user_id', $user);
        }
        if (isset($user_name)){
            $query->whereHas('user', function ($sub_query) use($user_name) {
                $sub_query->where('username', 'like', '%' . $user_name . '%');
            });
        }
        $total = (clone $query);
        if (isset($pageIndex) && isset($pageSize)) {
            $query->offset($pageIndex * $pageSize)->limit($pageSize);
        }
        $leaves = $query->with('user', 'approver', 'type')->get();
        $response->data = $leaves->toArray();
        $response->total = $total->count();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/leaves",
     *     summary="Insert/Update leaves",
     *     operationId="insert-update-leaves",
     *     tags={"Leaves"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID: ID of leaves if update",
     *         in="formData",
     *         name="id",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="User: User Id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="date_from: Leave start date",
     *         in="formData",
     *         name="date_from",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="date_to: Leave end date",
     *         in="formData",
     *         name="date_to",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Resason: Reason for leave",
     *         in="formData",
     *         name="reason",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Approver Id",
     *         in="formData",
     *         name="user_approve_id",
     *         required=false,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="Leave type",
     *         in="formData",
     *         name="type_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'user_id' => 'required',
            'date_from' => 'required',
            'date_to' => 'required',
            'type_id' => 'required',
            'user_approve_id' => 'nullable'
        ]);
        $data = $request->except('id');
        $leave = Leave::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        if($leave){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\DELETE(
     *     path="/leaves/{id}",
     *     summary="delete leave",
     *     operationId="delete-leave",
     *     tags={"Leaves"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of leaves",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function destroy(ApiResponse $response, $id){
        $leave = Leave::findOrFail($id);
        if($leave->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }

    /**
     * @SWG\GET(
     *     path="/leaves/remaining",
     *     summary="Get all remaining leaves",
     *     operationId="get-remiaining-leave",
     *     tags={"Leaves"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="Leave Id",
     *         in="query",
     *         name="leave_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function remleaveDays(ApiResponse $response){
        request()->validate([
            'user_id' => 'required',
            'leave_type_id' => 'required',
        ]);
        $user = request()->user_id;
        $leaveTypeId = request()->leave_type_id;
        $leaveType = LeaveType::find($leaveTypeId);
        $maxLeaveDays = $leaveType->max;
        $totalLeavesTaken = Leave::where([
            ['user_id', $user],
            ['type_id', $leaveTypeId],
        ]);
        if($maxLeaveDays > 0){
            $remainingLeaves = ( $maxLeaveDays - $totalLeavesTaken->count() ) ;
        }else if($maxLeaveDays == 0){
            $remainingLeaves = "The Leave type selected has an unlimited number of days";
        }
        $collection = collect([
            'user' => $user, 
            'maxLeaveDays' => $maxLeaveDays, 
            'totalLeavesTaken' => $totalLeavesTaken->count(), 
            'remainingLeaves' => $remainingLeaves
        ]);
        $response->data = $collection->toArray();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

        /**
     * @SWG\PUT(
     *     path="/leaves/status",
     *     summary="Update status",
     *     operationId="update-leave-status",
     *     tags={"Leaves Status"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Leave ID: ID of leave being updated",
     *         in="formData",
     *         name="leave_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     
     *       @SWG\Parameter(
     *         description="Status: new status of leave",
     *         in="formData",
     *         name="status",
     *         required=true,
     *         type="string",
     *         enum={"New","Approved", "Declined"}
     *     ),
     *       @SWG\Parameter(
     *         description="Id of User updating the leave status",
     *         in="formData",
     *         name="approver_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function updateLeaveStatus(ApiResponse $response, Request $request){
        $request->validate([
            'leave_id' => 'required',
            'status' => 'required|in:New,Approved,Declined',
            'approver_id' => 'required',
        ]);
        $leave = Leave::find($request->leave_id);
        $leave->status = $request->status;
        $leave->user_approve_id = $request->approver_id;
        if($leave->save()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }

    /**
     * @SWG\GET(
     *     path="/leave/{id}",
     *     summary="show leave",
     *     operationId="show-leave",
     *     tags={"Leaves"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of leave",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function show(ApiResponse $response, $id){
        $leave = Leave::find($id);
        $response->data = $leave;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }
}
