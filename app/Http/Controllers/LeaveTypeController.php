<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\LeaveTypeModel as LeaveType;

class LeaveTypeController extends Controller
{
    /**
     * @SWG\GET(
     *     path="/leave-types",
     *     summary="List leave Types",
     *     operationId="list-leave-types",
     *     tags={"Leave-types"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function get(ApiResponse $response){
        $leaveTypes = LeaveType::get();
        $response->data = $leaveTypes;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

}
