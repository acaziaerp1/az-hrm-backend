<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\Models\LocalizationSettingsModel as Localization;

class LocalizationSettingsController extends Controller
{
    /**
     * @SWG\GET(
     *     path="/settings/localization",
     *     summary="Show localization settings",
     *     operationId="get-localization-settings",
     *     tags={"Settings"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function index(ApiResponse $response){
        $localizationSettings = Localization::with('country')->first();
        if($localizationSettings){
            $response->data = $localizationSettings->toArray();
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/settings/localization",
     *     summary="Edit or create localization settings",
     *     operationId="edit-or-create-localization-settings",
     *     tags={"Settings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="country_id: Country ID",
     *         in="formData",
     *         name="country_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="language: language",
     *         in="formData",
     *         name="language",
     *         required= true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="timezone: timezone",
     *         in="formData",
     *         name="timezone",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="date_format: Date format",
     *         in="formData",
     *         name="date_format",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="currency_code: Currency code",
     *         in="formData",
     *         name="currency_code",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'country_id' => 'required|numeric',
            'date_format' => 'required',
            'timezone' => 'required',
            'language' => 'required',
            'currency_code' => 'required'
        ]);
        $data = $request->except('id');
        $localizationSettings = Localization::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        if($localizationSettings){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
