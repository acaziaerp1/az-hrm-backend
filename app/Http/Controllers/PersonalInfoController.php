<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ApiResponse;
use App\User;

class PersonalInfoController extends Controller
{
    /**
     * @SWG\GET(
     *     path="/personal-info",
     *     summary="Get Personal information",
     *     operationId="get-personal-info",
     *     tags={"Personal Information"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function get(ApiResponse $response){
        request()->validate([
            'user_id' => 'required | exists:App\User,id',
        ]);
        $user = User::where('id', request()->user_id)->with('reporter', 'department.head','country','city','province')->get();
        $response->data = $user;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/personal-info",
     *     summary="Get personal information",
     *     operationId="get-personal-info",
     *     tags={"Personal Information"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="user_id: User id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="email: User Email",
     *         in="formData",
     *         name="email",
     *         required= true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="username: username",
     *         in="formData",
     *         name="username",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="first_name: first_name",
     *         in="formData",
     *         name="first_name",
     *         required=true,
     *         type="string"
     *     ),
     *       @SWG\Parameter(
     *         description="last_name: last_name",
     *         in="formData",
     *         name="last_name",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="birth_date: Birth day",
     *         in="formData",
     *         name="birth_date",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="gender: gender",
     *         in="formData",
     *         name="gender",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="address: Address",
     *         in="formData",
     *         name="address",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="mobile_phone: Mobile Phone",
     *         in="formData",
     *         name="mobile_phone",
     *         required=true,
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         description="reporter_id: Reporter",
     *         in="formData",
     *         name="reporter_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="department: Department",
     *         in="formData",
     *         name="department_id",
     *         required=true,
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         description="Avatar: Avatar",
     *         in="formData",
     *         name="avatar",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'user_id' => 'required | exists:App\User,id',
            'email' => 'required',
            'username' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
            'birth_date' => 'required',
            'gender' => 'required',
            'address' => 'required',
            'mobile_phone' => 'required',
            'reporter_id' => 'required',
            'department_id' => 'required',
            'avatar' => 'image',
        ]);
        $data = $request->except('id');
        $user = User::update(
            ['id' => $request->get('id',0)],
            $data
        );
        if($user){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
