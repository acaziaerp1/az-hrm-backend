<?php

namespace App\Http\Controllers;

use App\Models\SalaryHistoryModel;
use App\User;
use App\Models\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Models\SalaryHistoryModel as SalaryHistory;

class SalaryHistoryController extends Controller
{
      /**
     * @SWG\GET(
     *     path="/salary-history",
     *     summary="Get user salary history",
     *     operationId="get-user-salary-history",
     *     tags={"Salary History"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="User Id",
     *         in="query",
     *         name="user_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function get(ApiResponse $response){
        request()->validate([
            'user_id' => 'required|exists:App\User,id'
        ]);
        $user =  User::find(request()->user_id)->salary_histories;
        $response->data = $user;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/salary-history",
     *     summary="Get bank information",
     *     operationId="get-bank-information",
     *     tags={"Bank Information"},
     *     produces={"application/json"},
     *       @SWG\Parameter(
     *         description="id: id",
     *         in="formData",
     *         name="id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="user_id: User id",
     *         in="formData",
     *         name="user_id",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="Effective Date: effective_date",
     *         in="formData",
     *         name="effective_date",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="Level: level",
     *         in="formData",
     *         name="level",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="salary: salary",
     *         in="formData",
     *         name="salary",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function store(ApiResponse $response, Request $request){
        $request->validate([
            'id' => 'sometimes|required',
            'user_id' => 'required|exists:App\User,id',
            'effective_date' => 'required',
            'level' => 'required',
            'salary' => 'required'
        ]);
        $salaryHistory = SalaryHistory::updateOrCreate(
            ['id' => $request->get('id',0)],
            [
                'user_id' => $request->user_id,
                'effective_date' => $request->effective_date,
                'level' => $request->level,
                'salary' => $request->salary
            ]
        );
        if ($salaryHistory) {
            $response->data = $request->toArray();
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

     /**
     * @SWG\DELETE(
     *     path="/salary-history/{id}",
     *     summary="delete salary-history",
     *     operationId="delete-salary-history",
     *     tags={"salary-history"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of salary-history",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function destroy(ApiResponse $response, $id)
    {
        $salaryHistory = SalaryHistoryModel::findOrFail($id);
        if($salaryHistory->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }
}
