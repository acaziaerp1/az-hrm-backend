<?php

namespace App\Http\Controllers;

use App\Models\AssetModel;
use App\Models\CategoryAssets;
use App\Models\TrainingModel;
use App\Models\TrainingTraineeModel;
use App\Models\TrainingTypeModel;
use Illuminate\Http\Request;
use App\User as UserModel;
use App\Models\ApiResponse;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class TrainingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\GET(
     *     path="/trainings",
     *     summary="Get all training",
     *     operationId="get-training",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="pageIndex: Current page of the query",
     *         in="query",
     *         name="pageIndex",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="pageSize: Limit of query",
     *         in="query",
     *         name="pageSize",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Trainer Name",
     *         in="query",
     *         name="trainer_name",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Category Training",
     *         in="query",
     *         name="type_id",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function get(ApiResponse $response)
    {
        $pageIndex = request()->pageIndex;
        $pageSize = request()->pageSize;
        $trainer_name = request()->trainer_name;
        $type_id = request()->type_id;
        $query = TrainingModel::with('trainee','category','owner')->orderBy('id', 'DESC');
        if (isset($trainer_name)){
            $query->where('trainer_name', 'like', '%' . $trainer_name . '%');
        }
        if (isset($type_id)){
            $query->whereHas('category', function ($sub_query) use($type_id) {
                $sub_query->where('id', $type_id);
            });
        }
        $total = (clone $query);
        if (isset($pageIndex) && isset($pageSize)) {
            $query->offset($pageIndex * $pageSize)->limit($pageSize);
        }
        $data = $query->get();
        $response->data = $data->toArray();
        $response->total = $total->count();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\GET(
     *     path="/trainings/detail/{id}",
     *     summary="Get detail",
     *     operationId="detail-training",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="Id of training",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function detail(ApiResponse $response,$id)
    {
        $data = TrainingModel::with('trainee','category','owner')->where('id',$id)->first();
        if($data){
            $response->data = $data;
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/trainings",
     *     summary="Insert/Update a training",
     *     operationId="insert-update-training",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID: ID of training if update",
     *         in="formData",
     *         name="id",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Type Id: Type training",
     *         in="formData",
     *         name="type_id",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Owner: Owner of training",
     *         in="formData",
     *         name="owner",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Trainer Name: Trainer name of training",
     *         in="formData",
     *         name="trainer_name",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="Start day of training (Y-m-d)",
     *         in="formData",
     *         name="start_date",
     *         required=false,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         description="End day of training (Y-m-d)",
     *         in="formData",
     *         name="end_date",
     *         required=false,
     *         type="string"
     *     ),
     *       @SWG\Parameter(
     *         description="Cost: Cost of training",
     *         in="formData",
     *         name="cost",
     *         required=false,
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         description="Is sponsor by company",
     *         in="formData",
     *         name="is_sponsor",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *       @SWG\Parameter(
     *         description="Status of training",
     *         in="formData",
     *         name="status",
     *         required=false,
     *         type="integer",
     *         enum={0,1}
     *     ),
     *       @SWG\Parameter(
     *         description="Description: Description of training",
     *         in="formData",
     *         name="description",
     *         required=true,
     *         type="string"
     *     ),
     *       @SWG\Parameter(
     *         description="Trainee of training",
     *         in="formData",
     *         name="trainees",
     *         required=true,
     *         type="string",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function store(Request $request,ApiResponse $response)
    {
        $request->validate([
            'id' => 'sometimes|required',
            'type_id' => 'required|numeric',
            'owner' => 'required|numeric',
            'trainer_name' => 'required',
            'description' => 'required',
            'trainees' => 'required|array',
            'start_date' => 'sometimes|nullable|date_format:Y-m-d',
            'end_date' => 'sometimes|nullable|date_format:Y-m-d',
        ]);
        $data = $request->except(['id','trainees']);
        $res = TrainingModel::updateOrCreate(
            ['id' => $request->get('id',0)],
            $data
        );
        //Sync ID trainee and id training
        $array_trainee = array();
        $related = TrainingModel::find($res->id);
        $ids = $related->traineeInTraining()->allRelatedIds()->toArray();
        $id_max = TrainingTraineeModel::max('id');
        $id_temp_max = $id_max;
        foreach($request->get('trainees') as $key => $item){
            $id_temp_max++;
            $id_temp = !empty($ids) && array_key_exists($key,$ids) ? $ids[$key] : $id_temp_max;
            $array_trainee[$id_temp] = array(
                'training_id' => $res->id,
                'trainee_id' => $item,
                'created_at' => time(),
                'updated_at' => time(),
            );
        }
        $sync_data = $related->traineeInTraining()->sync($array_trainee);
        if($sync_data){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\DELETE(
     *     path="/trainings/{id}",
     *     summary="delete trainings",
     *     operationId="delete-trainings",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of trainings",
     *         in="path",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function destroy(ApiResponse $response, $id)
    {
        try{
            $training = TrainingModel::find($id);
            $training->traineeInTraining()->detach();
            if($training->delete()){
                $response->success = true;
                $response->message = __('auth.success');
            }
        }catch(\Throwable $exception){
            $response->message = $exception->getMessage();
        }
        return response()->json($response, 200);
    }

    /**
     * @SWG\GET(
     *     path="/trainings/category",
     *     summary="get category trainings",
     *     operationId="category-trainings",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function category(ApiResponse $response)
    {
        $category_trainings = TrainingTypeModel::get();
        $response->data = $category_trainings;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\POST(
     *     path="/trainings/updateStatus",
     *     summary="update status trainings",
     *     operationId="status-trainings",
     *     tags={"Trainings"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="ID of trainings",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="status: trainings => 0 : New; 5 : Cancel; 10 : Done",
     *         in="formData",
     *         name="status_id",
     *         required=true,
     *         type="integer",
     *          enum={0,5,10}
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */

    public function updateStatus(Request $request,ApiResponse $response)
    {
        $request->validate([
            'status_id' => 'numeric|required',
            'id' => 'numeric|required',
        ]);
        $update = TrainingModel::where('id',$request->id)->update(['status' => $request->status_id]);
        if($update){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
