<?php


namespace App\Http\Controllers;

use App\Models\ApiResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UpdateCommonController extends Controller
{

    /**
     * @SWG\POST(
     *     path="/updateCommon/{nameModule}",
     *     summary="Update column in any table",
     *     operationId="update-any-column",
     *     tags={"UpdateCommon"},
     *     produces={"application/json"},
     *     consumes={"application/json"},
     *     @SWG\Parameter(
     *         description="nameModule",
     *         in="path",
     *         name="nameModule",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="Data",
     *          required=true,
     *          @SWG\Schema(
     *              @SWG\Property(
     *                  property="id",
     *                  type="integer",
     *                  default="1"
     *              ),
     *              @SWG\Property(
     *                  property="values",
     *                  type="integer",
     *                  default="array (key-value)"
     *              ),
     *          )
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     * security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function __invoke($objModel)
    {
        request()->validate(
            array(
                'id' => 'required|numeric',
                'values' => 'required|array',
            )
        );
        $response = new ApiResponse();
        try {
            $request = request()->all();
            $update = $objModel->where('id', (int)$request['id'])->update($request['values']);
            if ($update) {
                $response->success = true;
                $response->message = __('auth.success');
            }
        } catch (\Throwable $ex) {
            $response->message = $ex->getMessage();
        }
        return response()->json($response, 200);
    }

}
