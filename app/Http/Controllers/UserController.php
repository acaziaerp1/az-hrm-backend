<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User as UserModel;
use App\Models\ApiResponse;
use Carbon\Carbon;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /**
     * @SWG\GET(
     *     path="/users",
     *     summary="Get all user",
     *     operationId="get-user",
     *     tags={"User"},
     *     produces={"application/json"},
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     * )
     */

    public function old_get(ApiResponse $response)
    {
        $users = UserModel::get();
        $response->data = $users;
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\GET(
     *     path="/users",
     *     summary="search Users",
     *     operationId="show-leave",
     *     tags={"Users"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="searchKey: Search key",
     *         in="path",
     *         name="searchKey",
     *         required=false,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="pageIndex: Current page of the query",
     *         in="query",
     *         name="pageIndex",
     *         required=false,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="pageSize: Limit of query",
     *         in="query",
     *         name="pageSize",
     *         required=false,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function get(ApiResponse $response){
        $pageIndex = request()->pageIndex;
        $pageSize = request()->pageSize;
        $searchKey = request()->searchKey;
        $users = new UserModel();
        $query = $users->orderBy('id', 'DESC');
        if(isset($searchKey)){
            $query->where('username', 'like', "%{$searchKey}%");
        }
        $total = (clone $query);
        if (isset($pageIndex) && isset($pageSize)) {
            $query->skip($pageIndex * $pageSize)->take($pageSize);
        }
        $users = $query->get();
        $response->data = $users->toArray();
        $response->total = $total->count();
        $response->success = true;
        $response->message = __('auth.success');
        return response()->json($response, 200);
    }

    /**
     * @SWG\DELETE(
     *     path="/user/{id}",
     *     summary="delete user",
     *     operationId="delete-user",
     *     tags={"Users"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="id: id of user",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function destroy(ApiResponse $response, $id){
        $user = UserModel::findOrFail($id);
        if($user->delete()){
            $response->success = true;
            $response->message = __('auth.success');
            return response()->json($response, 200);
        }
    }

    /**
     * @SWG\PUT(
     *     path="/user/{id}",
     *     summary="update User information",
     *     operationId="update-user-info",
     *     tags={"Users"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         description="user_id: User id",
     *         in="formData",
     *         name="id",
     *         required=true,
     *         type="integer"
     *     ),
     *     @SWG\Parameter(
     *         description="email: User Email",
     *         in="formData",
     *         name="email",
     *         required= true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="first_name: first_name",
     *         in="formData",
     *         name="first_name",
     *         required=true,
     *         type="string"
     *     ),
     *       @SWG\Parameter(
     *         description="last_name: last_name",
     *         in="formData",
     *         name="last_name",
     *         required=true,
     *         type="string"
     *     ),
     *      @SWG\Parameter(
     *         description="gender: gender",
     *         in="formData",
     *         name="gender",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="mobile_phone: Mobile Phone",
     *         in="formData",
     *         name="mobile_phone",
     *         required=true,
     *         type="integer"
     *     ),
     *      @SWG\Parameter(
     *         description="department: Department",
     *         in="formData",
     *         name="department_id",
     *         required=true,
     *         type="integer"
     *     ),
     *       @SWG\Parameter(
     *         description="hire_date: Date hired",
     *         in="formData",
     *         name="hire_date",
     *         required=true,
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Successful operation",
     *     ),
     *      security={
     *       {"userToken": {}}
     *     }
     * )
     */
    public function update(ApiResponse $response, Request $request, $id){
        $request->validate([
            'email' => 'required|email',
            'first_name' => 'required',
            'last_name' => 'required',
            'gender' => 'required',
            'mobile_phone' => 'required',
            'hire_date' => 'required',
        ]);
        $user = UserModel::find($id);
        $user->email = $request->email;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->gender = $request->gender;
        $user->mobile_phone = $request->mobile_phone;
        $user->hire_date = $request->hire_date;
        if($user->save()){
            $response->success = true;
            $response->message = __('auth.success');
        }
        return response()->json($response, 200);
    }
}
