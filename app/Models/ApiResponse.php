<?php

namespace App\Models;

class ApiResponse
{
    function __construct() {
        $this->success = false;
    }

    public $message;
    public $success;
    public $data;
}
