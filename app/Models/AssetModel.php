<?php

namespace App\Models;

use App\Constants\ASSETSTATUS;
use Illuminate\Database\Eloquent\Model;
use App\User;

class AssetModel extends Model
{
    //
    protected $table = 'assets';
    protected $dateFormat = 'U';
    protected $fillable = ['id','name','code','description','user_id',
        'serial_number',
        'image',
        'date_loaned',
        'date_returned',
        'category_id',
    ];
    protected $appends = ['status_label'];


    public function getStatusLabelAttribute()
    {
        return ASSETSTATUS::getLabel($this->status);;
    }

    public function employee(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function category(){
        return $this->belongsTo('App\Models\CategoryAssets');
    }

    public function getImageAttribute($value)
    {
        if(!empty($value)){
            return asset('storage/' . $value);
        }
    }
}
