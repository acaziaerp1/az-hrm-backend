<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BankInfo extends Model
{
    protected $dateFormat = 'U';
    protected $fillable = [
        'user_id',
        'account_name',
        'account_number',
        'bank_name',
        'branch_name',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
