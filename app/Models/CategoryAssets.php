<?php

namespace App\Models;

use App\Constants\ASSETSTATUS;
use Illuminate\Database\Eloquent\Model;
use App\User;

class CategoryAssets extends Model
{
    //
    protected $table = 'category_assets';
    protected $dateFormat = 'U';

    public function assets(){
        return $this->hasMany('App\Models\AssetModel');
    }
}
