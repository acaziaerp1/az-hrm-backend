<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanySetting extends Model
{
    protected $table = 'setting_companies';
    protected $dateFormat = 'U';
    protected $hidden = array('city_id', 'country_id', 'province_id');

    public function city()
    {
        return $this->belongsTo('App\Models\City');
    }

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province');
    }
}
