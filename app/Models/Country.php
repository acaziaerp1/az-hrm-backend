<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $dateFormat = 'U';

    public function cities()
    {
        return $this->hasMany('App\Models\City');
    }
}
