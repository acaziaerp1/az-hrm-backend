<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Department extends Model
{
    //
    protected $dateFormat = 'U';
    public function head(){
        return $this->hasOne(User::class,'id','department_head');
    }

    public function employee(){
        return $this->hasMany(User::class,'department_id','id');
    }
}
