<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmailSettingsModel extends Model
{
    protected $table = 'setting_emails';
    protected $dateFormat = 'U';
    protected $fillable = [
        'smtp_host',
        'smtp_user',
        'smtp_password',
        'smtp_port',
        'smtp_security',
        'smtp_auth_domain',
    ];
}
