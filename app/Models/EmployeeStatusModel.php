<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeStatusModel extends Model
{
    protected $table = 'employee_status';
    public $timestamps = false;
    protected $fillable = [
        'status',
    ];
}