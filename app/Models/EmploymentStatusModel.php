<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmploymentStatusModel extends Model
{
    protected $table = 'employment_status';
    protected $dateFormat = 'U';
    protected $dates = [
        'effective_date',
    ];
    protected $fillable = [
        'user_id',
        'status_id',
        'effective_date',
        'comment',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status(){
        return $this->belongsTo('App\Models\EmployeeStatusModel', 'status_id');
    }
}
