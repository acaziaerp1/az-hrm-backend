<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobInformationModel extends Model
{
    protected $table = 'job_information';
    protected $dateFormat = 'U';
    protected $dates = [
        'effective_date',
    ];
    protected $fillable = [
        'user_id',
        'effective_date',
        'location',
        'division',
        'department',
        'job_title',
        'status_id',
        'reports_to',
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function reports_to(){
        return $this->belongsTo('App\User', 'reports_to');
    }

    public function status(){
        return $this->belongsTo('App\Models\EmployeeStatusModel', 'status_id');
    }
}
