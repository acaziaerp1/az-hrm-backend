<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobPositionsModel extends Model
{
    protected $table = 'job_positions';
    protected $dateFormat = 'U';
    protected $fillable = [
        'name',
    ];
}
