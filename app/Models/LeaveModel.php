<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveModel extends Model
{
    protected $table = 'leaves';
    protected $dateFormat = 'U';
    protected $hidden = array('user_id', 'user_approve_id', 'type_id');
    protected $fillable = [
        'user_id',
        'date_from',
        'date_to',
        'reason',
        'user_approve_id',
        'type_id',
    ];
    protected $dates = [
        'date_from',
        'date_to',
    ];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function approver(){
        return $this->belongsTo('App\User', 'user_approve_id');
    }

    public function type(){
        return $this->belongsTo('App\Models\LeaveTypeModel');
    }
}
