<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LeaveTypeModel extends Model
{
    protected $table = 'leave_types';
    protected $dateFormat = 'U';
    protected $fillable = [
        'name','type','max'
    ];
}
