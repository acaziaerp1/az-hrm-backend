<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LocalizationSettingsModel extends Model
{
    protected $table = 'setting_localizations';
    protected $dateFormat = 'U';
    protected $hidden = array('country_id',);
    protected $fillable = [
        'country_id',
        'date_format',
        'timezone',
        'language',
        'currency_code',
    ];

    public function country()
    {
        return $this->belongsTo('App\Models\Country');
    }
}
