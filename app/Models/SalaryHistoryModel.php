<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SalaryHistoryModel extends Model
{
    protected $table = 'salary_histories';
    protected $dateFormat = 'U';
    protected $dates = [
        'effective_date',
    ];
    protected $fillable = [
        'user_id',
        'effective_date',
        'level',
        'salary',
    ];
    // protected $casts = [
    //     'salary' => 'double',
    // ];

    public function user(){
        return $this->belongsTo('App\User');
    }
}
