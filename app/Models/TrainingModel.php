<?php

namespace App\Models;

use App\Constants\ASSETSTATUS;
use App\Constants\TRAININGSTATUS;
use Illuminate\Database\Eloquent\Model;
use App\User;

class TrainingModel extends Model
{
    //
    protected $table = 'trainings';
    protected $dateFormat = 'U';

    protected $fillable = [
        'id','type_id','owner','trainer_name','tags','cost',
        'start_date', 'end_date', 'description', 'status', 'is_company_sponsor',
    ];

    protected $appends = ['status_label'];


    public function getStatusLabelAttribute()
    {
        return TRAININGSTATUS::getLabel($this->status);;
    }

    public function trainee(){
        return $this->hasManyThrough(
            User::class,
            TrainingTraineeModel::class,
            'training_id',
            'id',
            'id',
            'trainee_id'
        );
    }

    public function category(){
        return $this->hasOne(TrainingTypeModel::class,'id','type_id');
    }
    public function owner(){
        return $this->hasOne(User::class,'id','owner');
    }

    public function traineeInTraining(){
        return $this->belongsToMany(TrainingTraineeModel::class,'training_trainees','training_id','id');
    }

}
