<?php

namespace App\Models;

use App\Constants\ASSETSTATUS;
use Illuminate\Database\Eloquent\Model;
use App\User;

class TrainingTraineeModel extends Model
{
    //
    protected $table = 'training_trainees';
    protected $dateFormat = 'U';

    protected $fillable = ['training_id', 'trainee_id','created_at','updated_at'];


//    public function trainee_training(){
//        return $this->belongsTo(TrainingTraineeModel::class,'training_id','id');
//    }
}
