<?php

namespace App\Models;

use App\Constants\ASSETSTATUS;
use Illuminate\Database\Eloquent\Model;
use App\User;

class TrainingTypeModel extends Model
{
    //
    protected $table = 'training_types';
    protected $dateFormat = 'U';
}
