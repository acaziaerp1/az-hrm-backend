<?php

namespace App;

use App\Models\City;
use App\Models\Country;
use App\Models\Province;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens, Notifiable;
    // public $timestamps = false;
    protected $dateFormat = 'U';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'email',
        'username',
        'first_name',
        'last_name',
        'birth_date',
        'gender',
        'address',
        'mobile_phone',
        'reporter_id',
        'department_id',
        'avatar',
        'created_at',
        'updated_at',
        'office_location',
        'address_2',
        'marital_status',
        'hire_date',
        'country_id',
        'city_id',
        'province_id',
        'employee_code',
        'employee_status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'integer',
        'updated_at' => 'integer',
    ];

    protected $dates = [
        'hire_date',
    ];

    public function leaves()
    {
        return $this->hasMany('App\Models\LeaveModel');
    }

    public function employmentStatus(){
        return $this->hasOne('App\Models\EmploymentStatusModel', 'user_id');
    }

    public function jobInformation(){
        return $this->hasMany('App\Models\JobInformationModel');
    }

    public function assets(){
        return $this->hasMany('App\Models\AssetModel');
    }

    public function reporter(){
        return $this->hasOne('App\User', 'reporter_id');
    }

    public function department(){
        return $this->belongsTo('App\Models\Department', 'department_id');
    }

    public function bank_info(){
        return $this->hasOne('App\Models\BankInfo');
    }

    public function salary_histories(){
        return $this->hasMany('App\Models\SalaryHistoryModel');
    }

    public function country(){
        return $this->belongsTo(Country::class,'country_id','id');
    }
    public function city(){
        return $this->belongsTo(City::class,'city_id','id');
    }
    public function province(){
        return $this->belongsTo(Province::class,'province_id','id');
    }
}
