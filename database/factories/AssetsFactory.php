<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\AssetModel::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'code' => sprintf('AST%05d', rand(1,9999999)),
        'description' => $faker->word,
        'user_id' => rand(1,5),
        'category_id' => rand(1,5),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
