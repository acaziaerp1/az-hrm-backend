<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\City::class, function (Faker $faker) {
    return [
        'city_name' => $faker->city,
        'country_id' => rand(1,30),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
