<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\CompanySetting::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'contact_person' => $faker->name,
        'address' => $faker->streetAddress,
        'country_id' => rand(1,15),
        'city_id' => rand(1,5),
        'province_id' => rand(1,25),
        'email' => $faker->unique()->safeEmail,
        'website_url' => $faker->domainName,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
