<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Country::class, function (Faker $faker) {
    return [
        'country_name' => $faker->country,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
