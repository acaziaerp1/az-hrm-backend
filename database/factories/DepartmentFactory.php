<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Department::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'department_head' => rand(1,5),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
