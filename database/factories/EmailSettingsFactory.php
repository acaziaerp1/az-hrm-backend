<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\EmailSettingsModel::class, function (Faker $faker) {
    return [
        'smtp_host' => 'smtp.mailtrap.io',
        'smtp_user' => '4100dc55e80232',
        'smtp_password' => '8ef6c6d78fd5e8',
        'smtp_port' => 2525,
        'smtp_security' => 1,
        'smtp_auth_domain' => 'smtp.mailtrap.io',
        'created_at' => now(),
        'updated_at' => now(),
    ];
});