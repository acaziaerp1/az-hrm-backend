<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\EmploymentStatusModel::class, function (Faker $faker) {
    return [
        'user_id' => $faker->unique()->numberBetween(1, App\User::count()),
        'effective_date' => $faker->dateTimeBetween('-3 years', 'now'),
        'status_id' => rand(1,4),
        'comment' => $faker->jobTitle,
    ];
});
