<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\Holiday::class, function (Faker $faker) {

    return [
        //
        'name' => $faker->text(5),
        'date' => $faker->dateTime($max = 'now', $timezone = null)
    ];
});
