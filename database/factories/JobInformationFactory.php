<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\JobInformationModel::class, function (Faker $faker) {
    return [
        'user_id' => rand(6,30),
        'effective_date' => $faker->dateTime($max = 'now', $timezone = null),
        'location' => $faker->city,
        'division' => "Western Cities",
        'department' => "HR",
        'job_title' => $faker->jobTitle,
        'status_id' => rand(1,4),
        'reports_to' => rand(1,5),
    ];
});
