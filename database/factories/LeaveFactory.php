<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\LeaveModel as Leave;

$factory->define(Leave::class, function (Faker $faker) {
    return [
        'user_id' => rand(6,30),
        'date_from' => $faker->dateTime($max = 'now', $timezone = null),
        'date_to' => $faker->dateTime($max = 'now', $timezone = null),
        'reason' => $faker->realText($maxNbChars = 200, $indexSize = 2),
        'user_approve_id' => rand(1,5),
        'type_id' => rand(1,3),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
