<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;
use App\Models\LocalizationSettingsModel as Localization;

$factory->define(Localization::class, function (Faker $faker) {
    return [
        'country_id' => 238,
        'date_format' => 'DD/MM/YY',
        'timezone' => +7,
        'language' => 'English',
        'currency_code' => 'VND',
        'created_at' => now(),
        'updated_at' => now()
    ];
});
