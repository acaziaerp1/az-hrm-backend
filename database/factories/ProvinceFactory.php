<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Province;
use Faker\Generator as Faker;

$factory->define(Province::class, function (Faker $faker) {
    return [
        'province_name' => $faker->state,
        'city_id' => rand(1,90),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
