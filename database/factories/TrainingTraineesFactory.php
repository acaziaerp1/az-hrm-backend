<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\TrainingTraineeModel::class, function (Faker $faker) {
    return [
        'training_id' => \App\Models\TrainingModel::all()->random()->id,
        'trainee_id' => \App\User::all()->random()->id,
        'created_at' => now(),
        'updated_at' => now()
    ];
});
