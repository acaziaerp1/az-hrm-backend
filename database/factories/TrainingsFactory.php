<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Models\TrainingModel::class, function (Faker $faker) {
    return [
        'type_id' => \App\Models\TrainingTypeModel::all()->random()->id,
        'owner' => \App\User::all()->random()->id,
        'description' => $faker->word,
        'trainer_name' => $faker->name,
        'cost' => $faker->randomFloat(2,0,200000),
        'created_at' => now(),
        'updated_at' => now()
    ];
});
