<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    $firstname = $faker->firstName;
    $lastname = $faker->lastName;
    $username = $firstname.'_'.$lastname;

    return [
        'username' => $username,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'first_name' => $firstname,
        'last_name' => $lastname,
        'birth_date' => date($format = 'Y-m-d'),
        'gender' => rand(0,1),
        'address' => $faker->address,
        // 'mobile_phone' => $faker->e164PhoneNumber,
        'reporter_id' => rand(1,10),
        'department_id' => rand(1,5),
        'employee_code' => 'acz-'.Str::random(7),
        'email' => $faker->unique()->safeEmail,
        // 'email_verified_at' => now(),
        'country_id' => \App\Models\Country::all()->random()->id,
        'city_id' => \App\Models\City::all()->random()->id,
        'province_id' => \App\Models\Province::all()->random()->id,
        'remember_token' => Str::random(10),
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
