<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('date_from')->nullable();
            $table->integer('date_to')->nullable();
            $table->text('reason')->nullable();
            $table->unsignedInteger('user_approve_id')->nullable();
            $table->foreign('user_approve_id')->references('id')->on('users');
            $table->unsignedInteger('type_id');
            $table->foreign('type_id')->references('id')->on('leave_types');
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
