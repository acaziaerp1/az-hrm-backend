<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingEmailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('smtp_host', 50)->nullable();
            $table->string('smtp_user', 50)->nullable();
            $table->string('smtp_password', 50)->nullable();
            $table->smallInteger('smtp_port')->nullable();
            $table->tinyInteger('smtp_security')->nullable();
            $table->string('smtp_auth_domain', 50)->nullable();
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_emails');
    }
}
