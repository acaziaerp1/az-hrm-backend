<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 50)->nullable();
            $table->string('subject', 255)->nullable();
            $table->unsignedInteger('assigner_id')->nullable();
            $table->unsignedInteger('assignee_id')->nullable();
            $table->unsignedInteger('client_id');
            $table->foreign('assigner_id')->references('id')->on('users');
            $table->foreign('assignee_id')->references('id')->on('users');
            $table->foreign('client_id')->references('id')->on('clients');
            $table->tinyInteger('priority')->nullable();
            $table->string('cc', 255)->nullable();
            $table->text('desription')->nullable();
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
