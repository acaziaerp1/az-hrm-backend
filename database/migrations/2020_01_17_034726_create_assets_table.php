<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100);
            $table->string('code', 50);
            $table->datetime('purchase_date')->nullable();
            $table->string('purchase_from', 50)->nullable();
            $table->string('manufacturer', 50)->nullable();
            $table->string('model', 50)->nullable();
            $table->string('serial_number', 50)->nullable();
            $table->string('supplier', 50)->nullable();
            $table->string('condition', 100)->nullable();
            $table->integer('warranty')->nullable();
            $table->decimal('value', 10, 0)->nullable();
            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->text('description');
            $table->tinyInteger('status')->nullable();
            $table->integer('created_at');
            $table->integer('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assets');
    }
}
