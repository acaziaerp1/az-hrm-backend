<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyToAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assets', function (Blueprint $table) {
            $table->dropColumn('purchase_date');
            $table->dropColumn('purchase_from');
            $table->dropColumn('manufacturer');
            $table->dropColumn('model');
            $table->dropColumn('supplier');
            $table->dropColumn('condition');
            $table->dropColumn('warranty');
            $table->dropColumn('value');
            $table->dropColumn('status');
            $table->date('date_loaned')->nullable();
            $table->date('date_returned')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assets', function (Blueprint $table) {

        });
    }
}
