<?php

use Illuminate\Database\Seeder;

class CategoryAssetsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_cat = ['Computer','Desk phone','Monitor','Mobile Phone','Television','Board'];
        foreach($arr_cat as $item){
            DB::table('category_assets')->insert([
                'name' => $item,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
    }
}
