<?php

use Illuminate\Database\Seeder;

class CompanySettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\CompanySetting::class, 1)->create();
    }
}
