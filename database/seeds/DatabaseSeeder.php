<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountriesTableSeeder::class);
        $this->call(ProvincesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(HolidaysSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(DepartmentsTableSeeder::class);
        $this->call(CompanySettingsTableSeeder::class);
        $this->call(CategoryAssetsTableSeeder::class);
        $this->call(AssetsSeeder::class);
        $this->call(TrainingTypeTableSeeder::class);
        $this->call(TrainingsTableSeeder::class);
        $this->call(TrainingTraineesTableSeeder::class);
        $this->call(LocalizationSeeders::class);
        $this->call(EmailSettingsSeeder::class);
        $this->call(LeaveTypeSeeder::class);
        $this->call(LeaveTableSeeder::class);
        $this->call(EmployeeStatusSeeder::class);
        $this->call(JobInformationSeeder::class);
        // $this->call(EmploymentStatusSeeder::class);
    }
}
