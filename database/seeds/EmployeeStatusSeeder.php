<?php

use Illuminate\Database\Seeder;
use App\Models\EmployeeStatusModel as EmployeeStatus;

class EmployeeStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('employee_status')->delete();

        $status = [
            ['status' => 'Full time'],
            ['status' => 'Part time'],
            ['status' => 'Probationary'],
            ['status' => 'Internship'],
        ];

        foreach($status as $status){
            EmployeeStatus::create($status);
        }
    }
}
