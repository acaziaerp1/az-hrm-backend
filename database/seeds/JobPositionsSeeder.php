<?php

use Illuminate\Database\Seeder;

class JobPositionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('job_positions')->delete();

        $status = [
            ['name' => 'Software engineer'],
            ['name' => 'SQA/Tester'],
            ['name' => 'Sales Manager'],
            ['name' => 'Customer Support Manager'],
            ['name' => 'HR Staff'],
            ['name' => 'HR Manager'],
            ['name' => 'Team Leader'],
            ['name' => 'Project Manager'],
            ['name' => 'Director'],
        ];

        foreach($status as $item){
            \App\Models\JobPositionsModel::create($item);
        }
    }
}
