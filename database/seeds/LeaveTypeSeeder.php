<?php

use Illuminate\Database\Seeder;
use App\Models\LeaveTypeModel as LeaveType;

class LeaveTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('leave_types')->delete();

        $leaveTypes = [
            ['name' => 'Personal leave', 'type' => 'unpaid', 'max' => 10],
            ['name' => 'Sick days', 'type' => 'Paid', 'max' => 10],
            ['name' => 'Vacation days', 'type' => 'Paid', 'max' => 12],
            ['name' => 'Compensatory off', 'type' => 'Compensatory', 'max' => 10],
            ['name' => 'Absent', 'type' => 'unpaid', 'max' => 10],
        ];

        foreach($leaveTypes as $leaveType){
            LeaveType::create($leaveType);
        }
    }
}
