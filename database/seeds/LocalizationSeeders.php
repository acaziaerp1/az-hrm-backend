<?php

use Illuminate\Database\Seeder;
use App\Models\LocalizationSettingsModel as Localization;

class LocalizationSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Localization::class, 1)->create();
    }
}
