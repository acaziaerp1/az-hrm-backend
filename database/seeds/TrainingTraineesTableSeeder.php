<?php

use Illuminate\Database\Seeder;

class TrainingTraineesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        factory(\App\Models\TrainingTraineeModel::class, 30)->create();
    }
}
