<?php

use Illuminate\Database\Seeder;

class TrainingTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $arr_cat = ['Internal'];
        foreach($arr_cat as $item){
            DB::table('training_types')->insert([
                'name' => $item,
                'created_at' => time(),
                'updated_at' => time()
            ]);
        }
    }
}
