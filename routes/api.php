<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//User API
Route::get('users', 'UserController@get');
Route::group([
    'prefix' => 'auth',
    'middleware' => ['cors'],
], function () {
    Route::post('authorized', 'AuthController@authorized');
    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
    });
});

Route::group([
    'middleware' => ['auth:api','cors'],
], function () {
    //Department API
    Route::get('departments', 'DepartmentController@index');
    Route::post('department', 'DepartmentController@store');
    Route::put('department', 'DepartmentController@store');
    Route::delete('department/{id}', 'DepartmentController@destroy');

    //Holiday API
    Route::get('holidays', 'HolidayController@index');
    Route::post('holiday', 'HolidayController@store');
    Route::put('holiday', 'HolidayController@store');
    Route::delete('holiday/{id}', 'HolidayController@destroy');

    //Address API
    Route::get('countries', 'AddressController@getCountries');
    Route::get('provinces', 'AddressController@getProvinces');
    Route::get('cities', 'AddressController@getCities');

    //Assets API
    Route::get('assets', 'AssetController@get');
    Route::get('assets/category', 'AssetController@category');
    Route::post('assets', 'AssetController@store');
    Route::delete('assets/{id}', 'AssetController@destroy');
    Route::get('user/assets', 'AssetController@userAssets');
    Route::post('assets/updateStatus', 'AssetController@updateStatus');
    // Settings API
    Route::get('settings', 'CompanySettingController@index');
    Route::put('settings', 'CompanySettingController@store');
    Route::post('settings', 'CompanySettingController@store');

    // Localization Settings API
    Route::get('settings/localization', 'LocalizationSettingsController@index');
    Route::put('settings/localization', 'LocalizationSettingsController@store');
    Route::post('settings/localization', 'LocalizationSettingsController@store');

    // Email Settings API
    Route::get('settings/email', 'EmailSettingsController@index');
    Route::put('settings/email', 'EmailSettingsController@store');
    Route::post('settings/email', 'EmailSettingsController@store');

    // LeaveTypes
    Route::get('leave-types', 'LeaveTypeController@get');

    // Leaves
    Route::get('leaves', 'LeaveController@get');
    Route::get('leaves/remaining', 'LeaveController@remleaveDays');
    Route::put('leaves', 'LeaveController@store');
    Route::put('leaves/status', 'LeaveController@updateLeaveStatus');
    Route::post('leaves', 'LeaveController@store');
    Route::delete('leaves/{id}', 'LeaveController@destroy');

    Route::get('leave/{id}', 'LeaveController@show');

    // Training API
    Route::get('trainings', 'TrainingController@get');
    Route::get('trainings/category', 'TrainingController@category');
    Route::get('trainings/detail/{id}', 'TrainingController@detail');
    Route::post('trainings', 'TrainingController@store');
    Route::post('trainings/updateStatus', 'TrainingController@updateStatus');
    Route::delete('trainings/{id}', 'TrainingController@destroy');

    // employment status API
    // Route::get('employment-status', 'EmploymentStatusController@get');
    // Route::put('employment-status', 'EmploymentStatusController@store');
    // Route::post('employment-status', 'EmploymentStatusController@store');

    // Job information API
    Route::get('job-information', 'JobInformationController@get');
    Route::get('job-title', 'JobInformationController@getJobTitle');
    Route::put('job-information', 'JobInformationController@store');
    Route::post('job-information', 'JobInformationController@store');
    Route::delete('job-information/{id}', 'JobInformationController@destroy');

    // Bank Info
    Route::get('bank-info', 'BankInfoController@index');
    Route::put('bank-info', 'BankInfoController@store');
    Route::post('bank-info', 'BankInfoController@store');
    Route::delete('bank-info/{id}', 'BankInfoController@destroy');

    // Personal Info
    Route::get('personal-info', 'PersonalInfoController@get');
    Route::put('personal-info', 'PersonalInfoController@store');

    // Salary History
    Route::get('salary-history', 'SalaryHistoryController@get');
    Route::put('salary-history', 'SalaryHistoryController@store');
    Route::post('salary-history', 'SalaryHistoryController@store');
    Route::delete('salary-history/{id}', 'SalaryHistoryController@destroy');

    // search User
    Route::get('users', 'UserController@get');
    Route::delete('user/{id}', 'UserController@destroy');
    Route::put('user/{id}', 'UserController@update');

    Route::post('updateCommon/{objModel}', 'UpdateCommonController');
});
