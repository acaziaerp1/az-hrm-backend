<?php
/**
 * @SWG\Swagger(
 *     schemes=API_SCHEMES,
 *     host=API_HOST,
 *     basePath="/api",
 *     @SWG\Info(
 *         version="1.0.0",
 *         title="Api document HRSM",
 *     ),
 * )
 */

/**
 * @SWG\Tag(
 *   name="Auth",
 *   description="Auth",
 * )
 */

/**
 * @SWG\SecurityScheme(
 *   securityDefinition="userToken",
 *   name="Authorization",
 *   type="apiKey",
 *   in="header",
 * )
 */
